## Wallet instructions
- First of All, thank you for taking the time to take our code challenge. We really appreciate it. And now, are you ready to rumble? :)
On localhost:XXXX/cc_test.png (public/cc_test.png) you'll find a picture which describes a basic wallet. This wallet consists in adding N Credit card numbers to a list. 
The actions are:
- Click on add a new card, it'll show 3 empty fields (to enter the Credit card number, CVV and expiration date) and a save button. The new number should be stored when the user clicks on the "Save" button.
- Each credit card on the list will have an X, in order to remove an item when it's needed.
- It's not possible to edit the items. 
- When you add a new credit card you will have to validate it against the payment processor (see the next section), the credit card will be only added if it's valid and it has funds.

## The payment processor
- Create a small payment processor, this will be a REST API. The payment processor will be capable to decide if a credit card is valid or not and if the credit card has funds.
- You have to model the communication between the wallet and the payment processor, do not send the CC in plain text (see the next section), determine a way to establish a secure communication between the wallet and the payment processor.

## Security concerns
We live in a dangerous world (seriously?) 
- The hacker role: Play the hacker role and try to find your code vulnerabilities
- Once you know your Weaknesses, design a safe way to store the credit cards. Remember even though you come up with a good idea, every system is vulnerable so you shouldn't allow that somebody can read the credit cards in the DB, in the case they take control of the system.
- There should be a way to retrieve a credit card from a customer without compromise the security and without knowing the entire Credit card number.
- The React JS and the API should communicate in a safe way to avoid for example CSRF. 


## Requirements to bear in mind
- Please, The wallet look and feel should be like cc_test.png. 
- This is an empty Rails 4.2.7 project.
- The ideal solution will use Rails as a REST API, and you will create a front end in React JS. As you may know, there is a react-rails implementation, if you are coming from a non-rails environment and you are already familiar with react JS, I DO NOT recommend the react-rails gem, you will find yourself wondering WHY GOD?
- There are several ways to solve this small challenge, be clever to ask the right questions and define your scope. 
- Tests are important, we use Rspec but mini test or whatever another framework will do the job. Unit tests are mandatory, but if you can add some functional ones it will be a plus.

### Notes
As you can see the requirement is pretty basic, the main idea behind this is to know you better, how you think, how you solve the problems. You know, the devil is in the details, so express yourself as a developer, use your techniques, your good practices, 
I think it's not necessary to balance a b-tree to show off.

### Installation
1. Clone this repository to your computer.
2. Run ```bundle install```.

### Experimenting
An interactive ruby console can be opened in a terminal by running:

    bundle exec pry